package com.example.playlist.net;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetroClient {
    private static final int CONNECTION_TIMEOUT = 30;

    private Retrofit mRestAdapter;
    private AkazooService akazooService;

    public RetroClient (Context context){
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        //tod o: fix the url problem
        mRestAdapter = new Retrofit.Builder()
                .baseUrl("http://akazoo.com/services/Test/TestMobileService.svc/")
                .client(getOkHttpClient(context))
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

    }

    public AkazooService getmVivaServiceServicee() {
        if(akazooService == null) {
            akazooService = mRestAdapter.create(AkazooService.class);
        }

        return akazooService;
    }

    private OkHttpClient getOkHttpClient(Context context){

        OkHttpClient.Builder okClientBuilder = new OkHttpClient.Builder();

        okClientBuilder.addInterceptor( new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request request = chain.request();


//                Headers.Builder headersBuilder = request.headers().newBuilder().add("Content-Type, application/json");
//

                // Add default query parameters
//                HttpUrl.Builder newUrlBuilder = request.url().newBuilder()
//                        .addQueryParameter("response", "application/json")
//                        .addQueryParameter("client", "android");


                return chain.proceed(request.newBuilder()
                        .header("Content-Type", "application/json")
                        .build()
                );
            }
        });

        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
            @Override public void log(String message) {
                Log.d( "httpLoggingInterceptor",message);
            }
        });
        httpLoggingInterceptor.setLevel( HttpLoggingInterceptor.Level.BODY);
        okClientBuilder.addInterceptor(httpLoggingInterceptor);

        okClientBuilder.connectTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS);
        okClientBuilder.readTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS);
        okClientBuilder.writeTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS);
        return okClientBuilder.build();
    }
}