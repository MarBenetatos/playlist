package com.example.playlist.net;



import com.example.playlist.model.PlaylistResp;
import com.example.playlist.model.PlaylistResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface AkazooService {

    @GET("playlists")
    Call<PlaylistResp> playlist();

    @GET("playlist")
    Call<PlaylistResponse> playlistinfo(
            @Query("playlistid") String playlistI);
}
