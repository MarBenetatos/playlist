package com.example.playlist.detailslist;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.playlist.model.Items;
import com.example.playlist.model.PlaylistResponse;
import com.example.playlist.net.RetroClient;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DetailsViewModel {

    private RetroClient client;
    private String id;
    private MutableLiveData<List<Items>> items;
    private CallEnd callEnd;

    public DetailsViewModel(Application context,String id,CallEnd callEnd) {
        this.client = new RetroClient(context);
        this.id = id;
        this.callEnd=callEnd;
    }

    public void init(){
       client.getmVivaServiceServicee().playlistinfo(id).enqueue(new Callback<PlaylistResponse>() {
           @Override
           public void onResponse(Call<PlaylistResponse> call, Response<PlaylistResponse> response) {
               items = new MutableLiveData<List<Items>>();
               items.setValue(response.body().getResult().getItems());
               callEnd.Callfinished();
           }
           @Override
           public void onFailure(Call<PlaylistResponse> call, Throwable t) {
            Log.d(" fail ",t.getMessage());
           }
       });

    }

    public LiveData<List<Items>> getAllItems() {
            return items;
    }
    public interface CallEnd{
        void Callfinished();
    }
}
