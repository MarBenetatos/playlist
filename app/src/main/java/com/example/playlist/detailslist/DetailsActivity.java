package com.example.playlist.detailslist;

import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;

import com.example.playlist.R;
import com.example.playlist.adapter.ItemAdapter;
import com.example.playlist.model.Items;
import com.example.playlist.uihelper.BaseActivity;

import java.util.List;

public class DetailsActivity extends BaseActivity implements DetailsViewModel.CallEnd {

    private DetailsViewModel detailsViewModel;
    private ItemAdapter itemAdapter;
    private RecyclerView mRecyvlerView;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        startLoading("Please wait");
        String id = getIntent().getStringExtra("PlaylistID");
        detailsViewModel = new DetailsViewModel(getApplication(), id, this);
        detailsViewModel.init();
        mRecyvlerView = findViewById(R.id.recycler_view);
        mRecyvlerView.setHasFixedSize(true);
        mRecyvlerView.setLayoutManager(new LinearLayoutManager(this));
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationIcon(R.drawable.ic_action_name);//Image Icon
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    @Override
    public void Callfinished() {
        detailsViewModel.getAllItems().observe(this, new Observer<List<Items>>() {
            @Override
            public void onChanged(List<Items> playlist_entities) {

                itemAdapter = new ItemAdapter(playlist_entities);
                mRecyvlerView.setAdapter(itemAdapter);
                stopLoading();

            }
        });
    }
}
