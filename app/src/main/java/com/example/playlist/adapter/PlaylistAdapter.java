package com.example.playlist.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.playlist.R;
import com.example.playlist.entity.Playlist_entity;

import java.util.List;

public class PlaylistAdapter extends RecyclerView.Adapter<PlaylistAdapter.ExampleViewHolder> {

        private List<Playlist_entity> mExampleList;
        private OnPlaylistClickListener mListener;

       public interface OnPlaylistClickListener {
            void onItemClick(int position, String id);
        }

        public void setOnItemClickListener(OnPlaylistClickListener listener) {
            mListener = listener;
        }

        public PlaylistAdapter(List<Playlist_entity> mExampleList) {
            this.mExampleList = mExampleList;
        }

        @NonNull
        @Override
        public ExampleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.playlistitem, parent, false);
            return new ExampleViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull final ExampleViewHolder holder, int position) {
            final Playlist_entity currentItem = mExampleList.get(position);
            holder.mTextViewCreator.setText(currentItem.getName());
            holder.mTrackCount.setText(currentItem.getItemCount());
        }

        @Override
        public int getItemCount() {
            return mExampleList.size();
        }

        public class ExampleViewHolder extends RecyclerView.ViewHolder {

            public TextView mTextViewCreator, mTrackCount;

            public ExampleViewHolder(@NonNull View itemView) {
                super(itemView);

                mTextViewCreator = itemView.findViewById(R.id.trackName);
                mTrackCount = itemView.findViewById(R.id.trackcount);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (mListener != null) {
                            int position = getAdapterPosition();
                            if (position != RecyclerView.NO_POSITION) {
                                mListener.onItemClick(position,mExampleList.get(position).getPlaylistId());

                            }
                        }
                    }
                });
            }
        }

        public void AdapterRefresh(List<Playlist_entity> mExampleList) {
            this.mExampleList = mExampleList;
            notifyDataSetChanged();
        }

}
