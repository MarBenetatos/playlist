package com.example.playlist.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.playlist.R;
import com.example.playlist.model.Items;

import java.util.List;

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.ExampleViewHolder> {

    private List<Items> mExampleList;
    private Context context;


    public ItemAdapter(List<Items> mExampleList) {
        this.mExampleList = mExampleList;
    }

    @NonNull
    @Override
    public ExampleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item, parent, false);
        return new ExampleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ExampleViewHolder holder, int position) {
        final Items currentItem = mExampleList.get(position);
        holder.mArtistName.setText(currentItem.getArtistName());
        holder.mTrackName.setText(currentItem.getTrackName());
        String test = currentItem.getImageUrl();
        RequestOptions requestOptions = new RequestOptions();
        Glide.with(context).load(test).apply(requestOptions.centerCrop()).into(holder.mimageView);
    }

    @Override
    public int getItemCount() {
        return mExampleList.size();
    }

    public class ExampleViewHolder extends RecyclerView.ViewHolder {

        public ImageView mimageView;
        public TextView mArtistName, mTrackName;

        public ExampleViewHolder(@NonNull View itemView) {
            super(itemView);
            mimageView = itemView.findViewById(R.id.image_view);
            mArtistName = itemView.findViewById(R.id.artistname);
            mTrackName = itemView.findViewById(R.id.trackname);

        }
    }
}
