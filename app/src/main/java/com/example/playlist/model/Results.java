package com.example.playlist.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Results {
    @SerializedName("Items")
    private List<Items> items;

    public List<Items> getItems() {
        return items;
    }
}
