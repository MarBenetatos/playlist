package com.example.playlist.model;




public class Result{

    private String DateUpdated;
    private String DateUpdatedIso;
    private int Duration;
    private int FanCount;
    private boolean IsOwner;
    private int ItemCount;
    private String LargePhotoUrl;
    private String Name;
    private String ObjectType;
    private String OwnerId;
    private String OwnerNickName;
    private String OwnerPhotoUrl;
    private String PhotoUrl;
    private String PlaylistId;
    private boolean ViewerIsFan;

    public String getDateUpdated() {
        return DateUpdated;
    }

    public String getDateUpdatedIso() {
        return DateUpdatedIso;
    }

    public int getDuration() {
        return Duration;
    }

    public int getFanCount() {
        return FanCount;
    }

    public boolean isOwner() {
        return IsOwner;
    }

    public int getItemCount() {
        return ItemCount;
    }

    public String getLargePhotoUrl() {
        return LargePhotoUrl;
    }

    public String getName() {
        return Name;
    }

    public String getObjectType() {
        return ObjectType;
    }

    public String getOwnerId() {
        return OwnerId;
    }

    public String getOwnerNickName() {
        return OwnerNickName;
    }

    public String getOwnerPhotoUrl() {
        return OwnerPhotoUrl;
    }

    public String getPhotoUrl() {
        return PhotoUrl;
    }

    public String getPlaylistId() {
        return PlaylistId;
    }

    public boolean isViewerIsFan() {
        return ViewerIsFan;
    }
}