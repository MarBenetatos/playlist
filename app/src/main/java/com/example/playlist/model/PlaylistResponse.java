package com.example.playlist.model;


import com.google.gson.annotations.SerializedName;

public class PlaylistResponse {
    @SerializedName("Result")
    private Results result;

    public Results getResult() {
        return result;
    }
}
