package com.example.playlist.model;

import com.example.playlist.entity.Playlist_entity;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PlaylistResp {
    @SerializedName("Result")
    private List<Playlist_entity> result;

    private String ErrorData;
    private int ErrorId;
    private Boolean IsError;

    public List<Playlist_entity> getResult() {
        return result;
    }
}
