package com.example.playlist.model;

public class Items {

    private String ImageUrl;
    private String ArtistName;
    private String TrackName;

    public String getImageUrl() {
        return ImageUrl;
    }

    public String getArtistName() {
        return ArtistName;
    }

    public String getTrackName() {
        return TrackName;
    }
}
