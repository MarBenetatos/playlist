package com.example.playlist.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.playlist.entity.Playlist_entity;

import java.util.List;


@Dao
public interface Playlist_Dao {

    @Insert
    void insert(Playlist_entity playlist_entity);


    @Update
    void update(Playlist_entity playlist_entity);

    @Delete
    void delete(Playlist_entity playlist_entity);

    @Query("DELETE FROM PLAYLIST_TAVLE")
    void DeleteAllPlaylist();

    @Query("Select * from playlist_tavle")
    LiveData<List<Playlist_entity>> getAllPlaylists();

//   @Insert
//    void insertAll(List<Playlist_entity>... lists);
}
