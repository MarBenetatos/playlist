package com.example.playlist.db;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.playlist.dao.Playlist_Dao;
import com.example.playlist.entity.Playlist_entity;

@Database(entities = {Playlist_entity.class},version = 1)
public abstract class PlaylistDataBase extends RoomDatabase {

    private static PlaylistDataBase instance;

    public abstract Playlist_Dao playlist_dao();

    public static synchronized PlaylistDataBase getInstance(Context context){
        if (instance==null){
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    PlaylistDataBase.class,"playlist_database")
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return  instance;
    }
}
