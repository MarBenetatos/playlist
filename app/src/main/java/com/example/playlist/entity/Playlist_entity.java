package com.example.playlist.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Date;

@Entity(tableName = "playlist_tavle")
public class Playlist_entity {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private String PlaylistId;
    private String Name;
    private int Duration;
    private String ItemCount;
//    private Boolean IsOwner;
    private String DateUpdated;
    private String DateUpdatedIso;
    private String OwnerId;
    private String OwnerNickName;
    private String OwneOwnerPhotoUrlrId;
    private Boolean ViewerIsFan;
    private int FanCount;
    private String PhotoUrl;
    private String LargePhotoUrl;
    private String ObjectType;

//    public Playlist_entity(String playlistId, String name, int duration, int itemCount, Boolean isOwner, String dateUpdated,
//                           String dateUpdatedIso, String ownerId, String ownerNickName, String owneOwnerPhotoUrlrId, Boolean viewerIsFan,
//                           int fanCount, String photoUrl, String largePhotoUrl, String objectType) {
//        PlaylistId = playlistId;
//        Name = name;
//        Duration = duration;
//        ItemCount = itemCount;
////        IsOwner = isOwner;
//        DateUpdated = dateUpdated;
//        DateUpdatedIso = dateUpdatedIso;
//        OwnerId = ownerId;
//        OwnerNickName = ownerNickName;
//        OwneOwnerPhotoUrlrId = owneOwnerPhotoUrlrId;
//        ViewerIsFan = viewerIsFan;
//        FanCount = fanCount;
//        PhotoUrl = photoUrl;
//        LargePhotoUrl = largePhotoUrl;
//        ObjectType = objectType;
//    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getPlaylistId() {
        return PlaylistId;
    }

    public String getName() {
        return Name;
    }

    public int getDuration() {
        return Duration;
    }

    public String getItemCount() {
        return ItemCount;
    }

//    public Boolean getIsOwner() {
//        return IsOwner;
//    }

    public String getDateUpdated() {
        return DateUpdated;
    }

    public String getDateUpdatedIso() {
        return DateUpdatedIso;
    }

    public String getOwnerId() {
        return OwnerId;
    }

    public String getOwnerNickName() {
        return OwnerNickName;
    }

    public String getOwneOwnerPhotoUrlrId() {
        return OwneOwnerPhotoUrlrId;
    }

    public Boolean getViewerIsFan() {
        return ViewerIsFan;
    }

    public int getFanCount() {
        return FanCount;
    }

    public String getPhotoUrl() {
        return PhotoUrl;
    }

    public String getLargePhotoUrl() {
        return LargePhotoUrl;
    }

    public String getObjectType() {
        return ObjectType;
    }

    public void setName(String name) {
        Name = name;
    }

    public void setDuration(int duration) {
        Duration = duration;
    }

    public void setItemCount(String itemCount) {
        ItemCount = itemCount;
    }

    public void setDateUpdated(String dateUpdated) {
        DateUpdated = dateUpdated;
    }

    public void setDateUpdatedIso(String dateUpdatedIso) {
        DateUpdatedIso = dateUpdatedIso;
    }

    public void setOwnerId(String ownerId) {
        OwnerId = ownerId;
    }

    public void setOwnerNickName(String ownerNickName) {
        OwnerNickName = ownerNickName;
    }

    public void setOwneOwnerPhotoUrlrId(String owneOwnerPhotoUrlrId) {
        OwneOwnerPhotoUrlrId = owneOwnerPhotoUrlrId;
    }

    public void setViewerIsFan(Boolean viewerIsFan) {
        ViewerIsFan = viewerIsFan;
    }

    public void setFanCount(int fanCount) {
        FanCount = fanCount;
    }

    public void setPhotoUrl(String photoUrl) {
        PhotoUrl = photoUrl;
    }

    public void setLargePhotoUrl(String largePhotoUrl) {
        LargePhotoUrl = largePhotoUrl;
    }

    public void setObjectType(String objectType) {
        ObjectType = objectType;
    }

    public void setPlaylistId(String playlistId) {
        PlaylistId = playlistId;
    }



}

