package com.example.playlist.uihelper;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.playlist.R;
import com.pnikosis.materialishprogress.ProgressWheel;


public class ProgressDialogFragment extends DialogFragment {
    public static final String EXTRA_MESSAGE = "EXTRA_MESSAGE";
    public static final String EXTRA_AUTO_DISMISS = "EXTRA_AUTO_DISMISS";
    public static final String EXTRA_ICON_RES_ID = "EXTRA_ICON_RES_ID";

    private String mProgressMessage;
    private boolean mAutoDismiss;
    private int mIconResId;

    private Dialog mProgressDialog;

    private TextView mProgressMessageView;
    private ProgressWheel mProgressWheel;
    private ImageView mIcon;

    public static ProgressDialogFragment newInstance(int iconResId, String progressMessage, boolean autoDismiss) {
        ProgressDialogFragment f = new ProgressDialogFragment();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putString(EXTRA_MESSAGE, progressMessage);
        args.putBoolean(EXTRA_AUTO_DISMISS, autoDismiss);
        args.putInt(EXTRA_ICON_RES_ID, iconResId);
        f.setArguments(args);

        return f;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if(mProgressDialog != null){
            return mProgressDialog;
        }

        mProgressDialog = new Dialog(getActivity(), R.style.ProgressDialogTheme);
        mProgressDialog.setContentView(R.layout.dialog_progress);
        mProgressDialog.setCanceledOnTouchOutside(false);

        mProgressWheel = mProgressDialog.findViewById(R.id.progress);
        mIcon = mProgressDialog.findViewById(R.id.icon);
        mProgressMessageView = mProgressDialog.findViewById(R.id.message);

        if (savedInstanceState == null) {
            setProgressMessage(getArguments().getString(EXTRA_MESSAGE, null));
            setIcon(getArguments().getInt(EXTRA_ICON_RES_ID, -1));
            setAutoDismiss(getArguments().getBoolean(EXTRA_AUTO_DISMISS));
        } else {
            setProgressMessage(savedInstanceState.getString(EXTRA_MESSAGE, null));
            setIcon(savedInstanceState.getInt(EXTRA_ICON_RES_ID, -1));
            setAutoDismiss(savedInstanceState.getBoolean(EXTRA_AUTO_DISMISS));
        }

        return (mProgressDialog);
    }

    public void setIcon(int iconResId) {
        if (!isInit()) return;
        mIconResId = iconResId;
        if (iconResId > 0) {
            mIcon.setVisibility(View.VISIBLE);
            mIcon.setImageResource(iconResId);

            mProgressWheel.setVisibility(View.GONE);
        } else if (iconResId == 0) {
            mIcon.setVisibility(View.GONE);
            mProgressWheel.setVisibility(View.VISIBLE);
        } else {
            mIcon.setVisibility(View.GONE);
            mProgressWheel.setVisibility(View.GONE);
        }
    }

    public void setProgressMessage(String progressMessage) {
        if (!isInit()) return;
        mProgressMessage = progressMessage;
        if (!TextUtils.isEmpty(progressMessage)) {
            mProgressMessageView.setVisibility(View.VISIBLE);
            mProgressMessageView.setText(mProgressMessage);
        } else {
            mProgressMessageView.setVisibility(View.GONE);
            mProgressMessageView.setText("");
        }
    }

    public void setAutoDismiss(boolean autoDismiss) {
        if (!isInit()) return;
        mAutoDismiss = autoDismiss;
        if (autoDismiss) {
            // we are to display a Toast like dialog
            mProgressWheel.setVisibility(View.GONE);
            if (mAutoDismiss && mProgressDialog.isShowing()) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (getDialog() != null) {
                            dismissAllowingStateLoss();
                        }
                    }
                }, 5000);
            }
            mProgressDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(final DialogInterface dialog) {
                    if (mAutoDismiss) {
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (dialog != null)
                                    dialog.dismiss();
                            }
                        }, 5000);
                    }
                }
            });
        } else {
            mProgressWheel.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        FragmentTransaction ft = manager.beginTransaction();
        ft.add(this, tag);
        ft.commitAllowingStateLoss();
    }

    public void dismissWithMessage(int iconResId, String message) {
        setIcon(iconResId);
        setProgressMessage(message);
        setAutoDismiss(true);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(EXTRA_MESSAGE, mProgressMessage);
        outState.putBoolean(EXTRA_AUTO_DISMISS, mAutoDismiss);
        outState.putInt(EXTRA_ICON_RES_ID, mIconResId);
        super.onSaveInstanceState(outState);
    }

    private boolean isInit() {
        return mIcon != null;
    }
}

