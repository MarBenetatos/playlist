package com.example.playlist.uihelper;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;



public class BaseActivity extends AppCompatActivity {
    private static final String PROGRESS_FRAGMENT_DIALOG_TAG = "PROGRESS_FRAGMENT_DIALOG_TAG";


    public void startLoading() {
        startLoading(null);
    }

    public void startLoading(String message) {
        ProgressDialogFragment dialogFragment = (ProgressDialogFragment) getSupportFragmentManager().findFragmentByTag(PROGRESS_FRAGMENT_DIALOG_TAG);

        // instantiate our dialog fragment if needed
        // otherwise use the dialog already displayed and just change it's message
        if(dialogFragment == null) {
            dialogFragment = ProgressDialogFragment.newInstance(-1, message, false);
            dialogFragment.show(getSupportFragmentManager(), PROGRESS_FRAGMENT_DIALOG_TAG);
        } else {
            dialogFragment.setProgressMessage(message);
        }
        dialogFragment.setCancelable(false);
    }

    public  void stopLoading(int iconResId, String message) {
        ProgressDialogFragment dialogFragment = (ProgressDialogFragment) getSupportFragmentManager().findFragmentByTag(PROGRESS_FRAGMENT_DIALOG_TAG);
        if(dialogFragment != null) {
            dialogFragment.dismissWithMessage(iconResId, message);
        }
    }

    public ProgressDialogFragment showMessage(int iconResId, String message) {
//        Toast.makeText(this, message, Toast.LENGTH_LONG).show();

        ProgressDialogFragment dialogFragment = (ProgressDialogFragment) getSupportFragmentManager().findFragmentByTag(PROGRESS_FRAGMENT_DIALOG_TAG);

        // kill existing dialog and show a new one
        if(dialogFragment != null) {
            dialogFragment.dismissAllowingStateLoss();
        }

        dialogFragment = ProgressDialogFragment.newInstance(iconResId, message, true);
        dialogFragment.show(getSupportFragmentManager(), PROGRESS_FRAGMENT_DIALOG_TAG);
        dialogFragment.setCancelable(false);

        return dialogFragment;
    }

    public void stopLoading () {
        ProgressDialogFragment dialogFragment = (ProgressDialogFragment) getSupportFragmentManager().findFragmentByTag(PROGRESS_FRAGMENT_DIALOG_TAG);
        if(dialogFragment != null) {
            dialogFragment.dismissAllowingStateLoss();
        }
    }
}
