package com.example.playlist.main;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.example.playlist.R;
import com.example.playlist.adapter.PlaylistAdapter;
import com.example.playlist.detailslist.DetailsActivity;
import com.example.playlist.entity.Playlist_entity;
import com.example.playlist.uihelper.BaseActivity;

import java.util.List;

public class MainActivity extends BaseActivity implements PlaylistAdapter.OnPlaylistClickListener {

    public static final String PlaylistID = "PlaylistID";

    private MainViewModel mainViewModel;
    private PlaylistAdapter playlistAdapter;
    private RecyclerView mRecyvlerView;
    private androidx.appcompat.widget.Toolbar toolbar;
    private SwipeRefreshLayout refreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mainViewModel = new ViewModelProvider(this).get(MainViewModel.class);

        mRecyvlerView = findViewById(R.id.recycler_view);
        mRecyvlerView.setHasFixedSize(true);
        mRecyvlerView.setLayoutManager(new LinearLayoutManager(this));
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mainViewModel.getAllPlaylists().observe(this, new Observer<List<Playlist_entity>>() {
            @Override
            public void onChanged(List<Playlist_entity> playlist_entities) {
                if (playlistAdapter == null) {
                    //init the app
                    playlistAdapter = new PlaylistAdapter(playlist_entities);
                    mRecyvlerView.setAdapter(playlistAdapter);
                    playlistAdapter.setOnItemClickListener(MainActivity.this);
                    if (playlist_entities.size()==0){
                        startLoading("Fetching Data");
                        mainViewModel.init();
                    }
                } else {

                    playlistAdapter.AdapterRefresh(playlist_entities);
                    stopLoading();
                }
                Log.d(" count " , String.valueOf(playlist_entities.size()));
            }
        });

        refreshLayout = findViewById(R.id.refresh);
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                startLoading("Refreshing Data");
                mainViewModel.deleteAll();
                mainViewModel.init();
                //to have many refreshes in the same view
                refreshLayout.setRefreshing(false);
            }
        });
    }


    public void onItemClick(int position, String id) {

        Log.d(" item clicked" , String.valueOf(position) +" "+ id);

        Intent detailIntent = new Intent(this, DetailsActivity.class);

        detailIntent.putExtra("PlaylistID", id);

        startActivity(detailIntent);
    }
}
