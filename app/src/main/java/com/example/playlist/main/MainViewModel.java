package com.example.playlist.main;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.example.playlist.entity.Playlist_entity;
import com.example.playlist.net.RetroClient;
import com.example.playlist.repo.PlaylistRepo;

import java.util.List;

public class MainViewModel extends AndroidViewModel {

    private PlaylistRepo playlistRepo;
    private LiveData<List<Playlist_entity>> allPlaylists;
    private RetroClient retroClient;

    public MainViewModel(@NonNull Application application) {
        super(application);

        playlistRepo = new PlaylistRepo(application);
        allPlaylists = playlistRepo.getAllplaylists();
        retroClient= new RetroClient(application);
    }

    public void init(){
            playlistRepo.FetchPlaylists(retroClient);
    }

    public void insert(Playlist_entity playlist_entity) {
        playlistRepo.insert(playlist_entity);
    }

    public void update(Playlist_entity playlist_entity) {
        playlistRepo.update(playlist_entity);
    }

    public void delete(Playlist_entity playlist_entity) {
        playlistRepo.delete(playlist_entity);
    }

    public void deleteAll() {
        playlistRepo.deleteall();
    }

    public LiveData<List<Playlist_entity>> getAllPlaylists() {
        return allPlaylists;
    }
}
