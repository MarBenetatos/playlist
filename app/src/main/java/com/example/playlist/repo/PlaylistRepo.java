package com.example.playlist.repo;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;

import androidx.lifecycle.LiveData;

import com.example.playlist.dao.Playlist_Dao;
import com.example.playlist.db.PlaylistDataBase;
import com.example.playlist.entity.Playlist_entity;
import com.example.playlist.model.PlaylistResp;
import com.example.playlist.net.RetroClient;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PlaylistRepo {

    private Playlist_Dao playlist_dao;
    private LiveData<List<Playlist_entity>> allplaylists;

    public PlaylistRepo(Application application){
        PlaylistDataBase playlistDataBase = PlaylistDataBase.getInstance(application);
        playlist_dao = playlistDataBase.playlist_dao();
        allplaylists = playlist_dao.getAllPlaylists();
    }

    public void insert(Playlist_entity playlist_entity){
        new InsertAsyncTask(playlist_dao).execute(playlist_entity);
    }

    public void update(Playlist_entity playlist_entity){
        new UpdateAsyncTask(playlist_dao).execute(playlist_entity);
    }

    public void delete(Playlist_entity playlist_entity){
        new DeleteAsyncTask(playlist_dao).execute(playlist_entity);
    }

    public void deleteall(){
        new DeleteAllAsyncTask(playlist_dao).execute();
    }

    public LiveData<List<Playlist_entity>> getAllplaylists() {
        return allplaylists;
    }
    //insert
    private static class InsertAsyncTask extends AsyncTask<Playlist_entity,Void,Void>{
        private Playlist_Dao playlist_dao;

        private InsertAsyncTask(Playlist_Dao playlist_dao){
            this.playlist_dao=playlist_dao;
        }

        @Override
        protected Void doInBackground(Playlist_entity... playlist_entities) {
            playlist_dao.insert(playlist_entities[0]);
            return null;
        }
    }

    //update
    private static class UpdateAsyncTask extends AsyncTask<Playlist_entity,Void,Void>{
        private Playlist_Dao playlist_dao;

        private UpdateAsyncTask(Playlist_Dao playlist_dao){
            this.playlist_dao=playlist_dao;
        }

        @Override
        protected Void doInBackground(Playlist_entity... playlist_entities) {
            playlist_dao.update(playlist_entities[0]);
            return null;
        }
    }

    //delete
    private static class DeleteAsyncTask extends AsyncTask<Playlist_entity,Void,Void>{
        private Playlist_Dao playlist_dao;

        private DeleteAsyncTask(Playlist_Dao playlist_dao){
            this.playlist_dao=playlist_dao;
        }

        @Override
        protected Void doInBackground(Playlist_entity... playlist_entities) {
            playlist_dao.delete(playlist_entities[0]);
            return null;
        }
    }

    //deleteAll
    private static class DeleteAllAsyncTask extends AsyncTask<Void,Void,Void>{
        private Playlist_Dao playlist_dao;

        private DeleteAllAsyncTask(Playlist_Dao playlist_dao){
            this.playlist_dao=playlist_dao;
        }

        @Override
        protected Void doInBackground(Void...voids) {
            playlist_dao.DeleteAllPlaylist();
            return null;
        }
    }

    //call
    public void FetchPlaylists(RetroClient client){

        client.getmVivaServiceServicee().playlist().enqueue(new Callback<PlaylistResp>() {
            @Override
            public void onResponse(Call<PlaylistResp> call, Response<PlaylistResp> response) {
                PlaylistResp playlistResp = response.body();

                for (int i=0;i<playlistResp.getResult().size();i++){

                    insert(playlistResp.getResult().get(i));
                }
            }

            @Override
            public void onFailure(Call<PlaylistResp> call, Throwable t) {
            Log.d(" fail ",t.getMessage());
            }
        });
    }

}
